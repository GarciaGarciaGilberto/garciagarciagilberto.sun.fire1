<?php 
$app->get('/api', function() use($app) { 
$datos = array( 
'titulo' => 'Censos Economicos', 
'recursos' => array( 
	array('url' => '/api', 'descripcion' => 'Este documento HTML'),
	array('url' => '/api/entidades', 'descripcion' => 'Un documento XML')
	array('url' => '/api/municipios', 'descripcion' => 'Un documento XML'),
	array('url' => '/api/temas', 'descripcion' => 'Un documento XML'),
	array('url' => '/api/identificadores', 'descripcion' => 'Un documento XML')
	) ); 
	$app->render('root.php', $datos); 
});