<?php


$app->get('/tema/xml', function() use($db,$app) {
  $datos = array(
    'books' => array(
    )
  );

  $books = $app->db->query("select * from tema");
  if (!$books) {
    $app->halt(500, "Error: En estos momentos no se puede acceder a la informacion de los temas disponibles.");
  }

  foreach ($books as $book) {
    $datos['books'][] = $book;
  }
  $app->response->headers->set('Content-Type', 'application/xml');
  $app->response->setStatus(200);
  $app->render('xml.php', $datos);
});

$app->get('/tema/xml/:id', function($id) use($db,$app) {
  $data = array(
    'books' => array(
    )
  );

if (id_invalido($id)) {
    $app->halt(400, "Error: El id '$id' no cumple con el formato correcto.");
  }

$id_tema=$id;

	$consulta="select * from tema where id_tema="+$id;
  $qry = $app->db->prepare("".$consulta);

  if ($qry->execute() === false) {
    $app->halt(500, "Error: No se ha podido encontrar el tema con id '$id'.");
  }

  if (!$qry) {
    $app->halt(500, "Error: En estos momentos no se puede acceder a la informacion.");
  }
	if ($qry->rowCount() ==0) {
    $app->halt(404, "Error: El tema con id '$id' no existe.");
  }

  foreach ($qry as $res) {
    $data['books'][] = $res;
  }
  $app->response->headers->set('Content-Type', 'application/xml');
  $app->response->setStatus(200);
  $app->render('xml.php', $data);
});

$app->delete('/tema/xml/:id', function($id) use($db,$app) {
  if (id_invalido($id)) {
    $app->halt(400, "Error: El id '$id' no cumple con el formato correcto.");
  } 
	$id_tema=$id;

  $consulta="delete from tema where id_tema='".$id_tema."'"; 
  $qry = $app->db->prepare($consulta);


  if ($qry->execute() === false) {
    $app->halt(500, "Error: No se ha podido borrar el tema con el id '$id'.");
  }
	if ($qry->rowCount() ==0) {
    $app->halt(404, "Error: El tema con id '$id' no existe.");
  }
  $app->halt(200, "Exito: El itema con id '$id' ha sido borrado");
});



$app->put('/tema/:id,:tem',function($id_usuario) use($db,$app) {

    $datosform=$app->request;
    $consulta=$db->prepare("update tema set id_tema=:id, tema="+$tem+
							" where id_tema=:id");
 
    $estado=$consulta->execute(
            array(
                ':id_tema'=>$id,
                ':tema'=> $tem->post('tema'),
              
                )
            );
 
    if ($consulta->rowCount()==1)
      echo json_encode(array('estado'=>true,'mensaje'=>'Datos actualizados correctamente.'));
    else
      echo json_encode(array('estado'=>false,'mensaje'=>'Error al actualizar datos, datos 
						no modificados o registro no encontrado.'));
});


