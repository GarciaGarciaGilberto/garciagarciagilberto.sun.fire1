<?php

function id_entidad_invalido($clave_entidad) {
  if (strlen($clave_entidad) != 1) {
    return true;
  }
}

/*Metodos entidad
metodo get
*/
$app->get('/entidades',function() use($app){
  $datos = array('entidades'=>array());
  $entidad=$app->db->query("select * from entidad");
  if(!$entidades){
    $app->halt(500, "Error: No se puede acceder a la informacion de entidades");
  }
  foreach($entidades as $entidades){
    $datos['entidades'][]=$entidades;
  }
  $app->response->headers->set('Content-Type','application/xml');
  $app->response->setStatus(200);
  $app->render('xml.php',$datos);
});


$app->get('/entidades/:cve',function($cve) use($app){
  $datos = array('entidades'=>array());
  $entidad=$app->db->query("select * from entidad where cve_entidad='$cve'");
  if(!$entidades){
    $app->halt(500, "Error: No se puede acceder a la informacion de entidades");
  }
  foreach($entidades as $entidades){
    $datos['entidades'][]=$entidades;
  }
  $app->response->headers->set('Content-Type','application/xml');
  $app->response->setStatus(200);
  $app->render('xml.php',$datos);
});





/*Eliminacion*/
$app->delete('/entidades/:clave_entidad', function($clave_entidad) use($app) {

  $entidad = $app->db->query("delete from entidad where cve_entidad='$clave_entidad'");

  if (!$entidad) {
    $app->halt(500, "Error: No se pudo eliminar la entidad correspondiente a la clave :'$cve_entidad' ");
  }

  if ($entidad->rowCount() != 1) {
    $app->halt(404, "Error:La entidad con la clave	:'$cve_entidad' no existe.");
  }
  $app->halt(200, "OK:La entidad con la clave:'$cve_entidad' ha sido borrado");
});


/*metodo post*/
$app->post('/entidades/:ent/:desc', function($ent,$desc) use($app) {

  $entidad = $app->db->query("select * from entidad where cve_entidad='$ent'");
 
  if ($entidad->rowCount() >= 1) {
    $app->halt(404, "Error:Ya existe una entrada con ese identificador.");
  }
  else{
    $ultimo_valor= $app->db->query("select max(cve_entidad) as id_entidad from entidad");
    foreach ($ultimo_valor as $id){
      $nuevo=$id['id_entidad']+1;
    }
    $entidad = $app->db->query("insert into entidad values('$nuevo','$desc')");
    $app->halt(200, "OK: La entidad '$desc' ha sigo agregada satisfactoriamente");
  }
});


/*Actualizacion*/
$app->put('/entidades/:cve_entidad/:desc_entidad', function($cve_entidad,$desc_entidad) use($app)  {

 $entidad = $app->db->query("update entidad set desc_entidad'$desc_entidad' where cve_entidad='$cve_entidad' ");
  
  if (!$entidad) {
    $app->halt(500, "Error: No se ha podido actualizar la entidad correspondiente a la clave :'$cve_entidad' ");
  }

  if ($entidad->rowCount() != 1) {
    $app->halt(404, "Error: :No se ha podido actualizar la entidad correspondiente a la clave :'$cve_entidad'  no existe.");
  }
  $app->halt(200, "OK: Se actualizo la descripcion de la entidad con la clave:'$cve_entidad' ");
});
/*Fin de metodos entidad*/



/*Metodos municipio
metodo get
*/
$app->get('entidades/:clave_entidad/municipios',function($clave_entidad) use($app){
  $datos = array('municipio'=>array());
  $municipio=$app->db->query("select * from municipio where cve_entidad='$clave_entidad'");
  if(!$municipio){
    $app->halt(500, "Error: No se puede acceder a la informacion de municipios");
  }
  foreach($municipio as $municipio){
    $datos['municipio'][]=$municipio;
  }
  $app->response->headers->set('Content-Type','application/xml');
  $app->response->setStatus(200);
  $app->render('xml.php',$datos);
});



$app->get('entidades/:clave_entidad/municipios/:cve',function($clave_entidad,$cve) use($app){
  $datos = array('municipio'=>array());
  $municipio=$app->db->query("select * from municipio where cve_entidad='$clave_entidad' and cve_municipio='$cve'");
  if(!$municipio){
    $app->halt(500, "Error: No se puede acceder a la informacion de municipios");
  }
  foreach($municipio as $municipio){
    $datos['municipio'][]=$municipio;
  }
  $app->response->headers->set('Content-Type','application/xml');
  $app->response->setStatus(200);
  $app->render('xml.php',$datos);
});


/*Eliminacion*/
$app->delete('/entidades/:clave_entidad/municipios/:clave_municipio', function($clave_entidad,$clave_municipio) use($app) {

  $municipio = $app->db->query("delete from municipio where cve_municipio='$clave_municipio' and cve_entidad='$clave_entidad'");

  if (!$municipio) {
    $app->halt(500, "Error: No se pudo eliminar la municipio correspondiente a la clave :'$cve_municipio' ");
  }

  if ($municipio->rowCount() != 1) {
    $app->halt(404, "Error:La municipio con la clave	:'$cve_municipio' no existe en la entidad con el id.'$clave_entidad'");
  }
  $app->halt(200, "OK:La municipio con la clave:'$cve_municipio' ha sido borrado");
});


/*metodo post*/
$app->post('/entidades/:clave_entidad/municipios/:clave_municipio/:desc_municipio', function($clave_entidad,$clave_municipio,$desc_municipio) use($app) {

  $municipio = $app->db->query("select * from municipio where cve_entidad='$clave_entidad'  and cve_municipio='$cve_municipio'");
 
  $entidad=$app->db->query("select * from entidad where cve_entidad='$clave_entidad'");
   if ($entidad->rowCount() < 1) {
    $app->halt(404, "Error:No existe una entidad con el identificador '$clave_entidad'");
  }
  else {
 if ($municipio->rowCount() >= 1) {
    $app->halt(404, "Error:Ya existe una entrada con ese identificador.");
  }
  
  else{
    $ultimo_valor= $app->db->query("select max(cve_municipio) as id_municipio from municipio where cve_entidad='$clave_entidad'");
    foreach ($ultimo_valor as $id){
      $nuevo=$id['id_municipio']+1;
    }
    $municipio = $app->db->query("insert into municipio values('$nuevo','$desc_municipio','$clave_entidad')");
    $app->halt(200, "OK: La municipio '$ent' ha sigo agregada satisfactoriamente");
  }}
});


/*Actualizacion*/
$app->put('/entidades/:cve_entidad/municipios/:cve_municipio/:desc_municipio', function($cve_entidad,$cve_municipio,$desc_municipio) use($app)  {

 $descripcion = $app->db->query("update municipio set desc_municipio'$desc_municipio' where cve_municipio='$cve_municipio'  and cve_entidad='$cve_entidad'");
  
  
  $entidad=$app->db->query("select * from entidad where cve_entidad='$cve_entidad'");
   if ($entidad->rowCount() < 1) {
    $app->halt(404, "Error:No existe una entidad con el identificador '$cve_entidad'");
  }
  else {
  if (!$descripcion) {
    $app->halt(500, "Error: No se ha podido actualizar la municipio correspondiente a la clave :'$cve_municipio' ");
  }

  if ($descripcion->rowCount() != 1) {
    $app->halt(404, "Error: :No se ha podido actualizar la municipio correspondiente a la clave :'$cve_municipio'  no existe.");
  }
  $app->halt(200, "OK: Se actualizo la descripcion de la municipio con la clave:'$cve_municipio' ");

  }
  
  
  });
/*Fin de metodos municipio*/



/*Metodos tema
metodo get
*/
$app->get('/temas',function() use($app){
  $datos = array('tema'=>array());
  $tema=$app->db->query("select * from tema");
  if(!$tema){
    $app->halt(500, "Error: No se puede acceder a la informacion de temas");
  }
  foreach($tema as $tema){
    $datos['tema'][]=$tema;
  }
  $app->response->headers->set('Content-Type','application/xml');
  $app->response->setStatus(200);
  $app->render('xml.php',$datos);
});



$app->get('/temas/:id',function($id) use($app){
  $datos = array('tema'=>array());
  $tema=$app->db->query("select * from tema  where id_tema='$id'");
  if(!$tema){
    $app->halt(500, "Error: No se puede acceder a la informacion de temas");
  }
  foreach($tema as $tema){
    $datos['tema'][]=$tema;
  }
  $app->response->headers->set('Content-Type','application/xml');
  $app->response->setStatus(200);
  $app->render('xml.php',$datos);
});



/*Eliminacion*/
$app->delete('/temas/:id_tema', function($id_tema) use($app) {

  $tema = $app->db->query("delete from tema where id_tema='$id_tema'");

  if (!$tema) {
    $app->halt(500, "Error: No se pudo eliminar la tema correspondiente a la id :'$id_tema' ");
  }

  if ($tema->rowCount() != 1) {
    $app->halt(404, "Error:El tema con la id	:'$id_tema' no existe.");
  }
  $app->halt(200, "OK:El tema con la id:'$id_tema' ha sido borrado");
});


/*metodo post*/
$app->post('/temas/:ent/:valor', function($ent,$valor) use($app) {

  $tema = $app->db->query("select * from tema where id_tema='$ent'");
 
  if ($tema->rowCount() >= 1) {
    $app->halt(404, "Error:Ya existe una entrada con ese identificador.");
  }
  else{
    $ultimo_valor= $app->db->query("select max(id_tema) as id_tema from tema");
    foreach ($ultimo_valor as $id){
      $nuevo=$id['id_tema']+1;
    }
    $tema = $app->db->query("insert into tema values('$nuevo','$valor')");
    $app->halt(200, "OK: El tema '$valor' ha sigo agregada satisfactoriamente");
  }
});


/*Actualizacion*/
$app->put('/temas/:id_tema/:desc_tema', function($id_tema,$desc_tema) use($app)  {

 $tema = $app->db->query("update tema set desc_tema'$desc_tema' where id_tema='$id_tema' ");
  
  if (!$tema) {
    $app->halt(500, "Error: No se ha podido actualizar el tema correspondiente a la id :'$id_tema' ");
  }

  if ($tema->rowCount() != 1) {
    $app->halt(404, "Error: :No se ha podido actualizar el tema correspondiente a la id :'$id_tema'  no existe.");
  }
  $app->halt(200, "OK: Se actualizo la descripcion del tema con la id:'$id_tema' ");
});
/*Fin de metodos tema*/

<?
/*Metodos identificador
metodo get
*/
$app->get('/identificadores',function() use($app){
  $datos = array('identificador'=>array());
  $identificador=$app->db->query("select * from identificador");
  if(!$identificador){
    $app->halt(500, "Error: No se puede acceder a la informacion de identificadores");
  }
  foreach($identificador as $identificador){
    $datos['identificador'][]=$identificador;
  }
  $app->response->headers->set('Content-Type','application/xml');
  $app->response->setStatus(200);
  $app->render('xml.php',$datos);
});

$app->get('/identificadores/:id',function($id) use($app){
  $datos = array('identificador'=>array());
  $identificador=$app->db->query("select * from identificador where id_identificador='$id'");
  if(!$identificador){
    $app->halt(500, "Error: No se puede acceder a la informacion de identificadores");
  }
  foreach($identificador as $identificador){
    $datos['identificador'][]=$identificador;
  }
  $app->response->headers->set('Content-Type','application/xml');
  $app->response->setStatus(200);
  $app->render('xml.php',$datos);
});



/*Eliminacion*/
$app->delete('/identificadores/:id_identificador', function($id_identificador) use($app) {

  $identificador = $app->db->query("delete from identificador where id_identificador='$id_identificador'");

  if (!$identificador) {
    $app->halt(500, "Error: No se pudo eliminar la identificador correspondiente a la id :'$id_identificador' ");
  }

  if ($identificador->rowCount() != 1) {
    $app->halt(404, "Error:El identificador con la id	:'$id_identificador' no existe.");
  }
  $app->halt(200, "OK:El identificador con la id:'$id_identificador' ha sido borrado");
});


/*metodo post*/
$app->post('/identificadores/:ent/:valor', function($ent,$valor) use($app) {

  $identificador = $app->db->query("select * from identificador where id_identificador='$ent'");
 
  if ($identificador->rowCount() >= 1) {
    $app->halt(404, "Error:Ya existe una entrada con ese identificador.");
  }
  else{
    $ultimo_valor= $app->db->query("select max(id_identificador) as id_identificador from identificador");
    foreach ($ultimo_valor as $id){
      $nuevo=$id['id_identificador']+1;
    }
    $identificador = $app->db->query("insert into identificador values('$nuevo','$valor')");
    $app->halt(200, "OK: El identificador '$valor' ha sigo agregada satisfactoriamente");
  }
});


/*Actualizacion*/
$app->put('/identificadores/:id_identificador/:desc_identificador', function($id_identificador,$desc_identificador) use($app)  {

 $identificador = $app->db->query("update identificador set desc_identificador'$desc_identificador' where id_identificador='$id_identificador' ");
  
  if (!$identificador) {
    $app->halt(500, "Error: No se ha podido actualizar el identificador correspondiente a la id :'$id_identificador' ");
  }

  if ($identificador->rowCount() != 1) {
    $app->halt(404, "Error: :No se ha podido actualizar el identificador correspondiente a la id :'$id_identificador'  no existe.");
  }
  $app->halt(200, "OK: Se actualizo la descripcion del identificador con la id:'$id_identificador' ");
});
/*Fin de metodos identificador*/





/*Metodos censo
metodo get
*/
$app->get('/entidades/:cve_entidad/municipios/:cve_municipio/tema1/:tem1/tema2/:tem2/tema3/:tem3/identificadores/:identificador',
function($cve_entidad,$cve_municipio,$tem1,$tem2,$tem3,$identificador) use($app){
  $datos = array('censo'=>array());
  $censo=$app->db->query("select * from censo where
  cve_entidad='$cve_entidad'  and cve_municipio=$cve_municipio
  
  and tema1='$tem1'
  and tema2='$tem2'
  and tema3='$tem3'
  and id_identificador=$identificador 
  
  ");
  if(!$censo){
    $app->halt(500, "Error: No se puede acceder a la informacion de censos");
  }
  foreach($censo as $censo){
    $datos['censo'][]=$censo;
  }
  $app->response->headers->set('Content-Type','application/xml');
  $app->response->setStatus(200);
  $app->render('xml.php',$datos);
});


/*Eliminacion*/
$app->delete('/entidades/:cve_entidad/municipios/:cve_municipio/tema1/:tem1/tema2/:tem2/tema3/:tem3/identificadores/:identificador',
function($cve_entidad,$cve_municipio,$tem1,$tem2,$tem3,$identificador) use($app) {

  $censo = $app->db->query("delete from censo where 
  
  
    cve_entidad='$cve_entidad'  and cve_municipio=$cve_municipio
  
  and tema1='$tem1'
  and tema2='$tem2'
  and tema3='$tem3'
  and id_identificador=$identificador 
  ");

  if (!$censo) {
    $app->halt(500, "Error: No se pudo eliminar la censo correspondiente  ");
  }

  if ($censo->rowCount() != 1) {
    $app->halt(404, "Error:El censo  no existe.");
  }
  $app->halt(200, "OK:El censo  ha sido borrado");
});


/*metodo post*/
$app->post('/entidades/:cve_entidad/municipios/:cve_municipio/tema1/:tem1/tema2/:tem2/tema3/:tem3/identificadores/:identificador/:valor',
function($cve_entidad,$cve_municipio,$tem1,$tem2,$tem3,$identificador,$valor) use($app) {

  $censo = $app->db->query("select * from censo where 
  
    
  
    cve_entidad='$cve_entidad'  and cve_municipio=$cve_municipio
  
  and tema1='$tem1'
  and tema2='$tem2'
  and tema3='$tem3'
  and id_identificador=$identificador 
  ");
 
  if ($censo->rowCount() >= 1) {
    $app->halt(404, "Error:Ya existe una entrada con ese idcenso.");
  }
  else{
    $ultimo_valor= $app->db->query("select max(id_censo) as id_censo from censo");
    foreach ($ultimo_valor as $id){
      $nuevo=$id['id_censo']+1;
    }
    $censo = $app->db->query("insert into censo values('$nuevo','$valor')");
    $app->halt(200, "OK: El censo '$id_censo' ha sigo agregada satisfactoriamente");
  }
});


/*Actualizacion*/
$app->put('/entidades/:cve_entidad/municipios/:cve_municipio/tema1/:tem1/tema2/:tem2/tema3/:tem3/identificadores/:identificador/:valor',
function($cve_entidad,$cve_municipio,$tem1,$tem2,$tem3,$identificador,$valor) use($app)  {

 $censo = $app->db->query("update censo set desc_censo'$valor' where 


  
  
    cve_entidad='$cve_entidad'  and cve_municipio=$cve_municipio
  
  and tema1='$tem1'
  and tema2='$tem2'
  and tema3='$tem3'
  and id_identificador=$identificador  ");
  
  if (!$censo) {
    $app->halt(500, "Error: No se ha podido actualizar el censo correspondiente  ");
  }

  if ($censo->rowCount() != 1) {
    $app->halt(404, "Error: :No se ha podido actualizar el censo correspondiente .");
  }
  $app->halt(200, "OK: Se actualizo la descripcion del censo");
});
/*Fin de metodos censo*/

